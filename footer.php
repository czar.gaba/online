<div class="before-footer">
    <div class="container">
        <div class="col-md-3 social-container">
            <h3>Follow us</h3>
            <ul class="social">
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            </ul>
        </div>
        <div class="col-md-9" class="announcement-container">
            <h3 class="announcement-title">LATEST NEWS AND ANNOUNCEMENTS <i class="fa fa-check" aria-hidden="true"></i>
            </h3>
       <article>
         <div class="well">
      <div class="media">
      	<a class="pull-left" href="#">
    		<img class="media-object" src="http://placehold.it/150x150">
  		</a>
  		<div class="media-body">
    		<h4 class="media-heading">Title</h4>
          <p class="text-right">By Natasha</p>
          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 
Quisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis 
dolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. 
Aliquam in felis sit amet augue.</p>
          <ul class="list-inline list-unstyled">
<!--
  			<li><span><i class="glyphicon glyphicon-calendar"></i> 2 days, 8 hours </span></li>
            <li>|</li>
            <span><i class="glyphicon glyphicon-comment"></i> 2 comments</span>
            <li>|</li>
            <li>
               <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star"></span>
                        <span class="glyphicon glyphicon-star-empty"></span>
            </li>
            <li>|</li>
-->
            <li>
            <!-- Use Font Awesome http://fortawesome.github.io/Font-Awesome/ -->
              <span><i class="fa fa-facebook-square"></i></span>
              <span><i class="fa fa-twitter-square"></i></span>
              <span><i class="fa fa-google-plus-square"></i></span>
            </li>
			</ul>
       </div>
    </div>
  </div>
       </article>
       
        </div>
    </div>
</div>

<footer>
    <div class="container">
        <div class="col-lg-6 col-md-6 col-sm-12">
							<div class="copyright">
								<p> Travel and Tours. Copyright <?php echo date('Y'); ?> . All Rights Reserved.</p>
								
							</div>
						</div>
    </div>
</footer>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/js.js"></script>

</body>
</html>